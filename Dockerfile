FROM python:2-alpine
MAINTAINER Fredrik Rambris "fredrik@rambris.com"
COPY app /app/
COPY requirements.txt docker/settings.cfg docker/supervisord.conf /app/
RUN apk update
RUN apk add nginx supervisor uwsgi uwsgi-python
RUN mkdir /run/nginx
RUN chown nginx:nginx /run/nginx
COPY docker/nginx.conf /etc/nginx/
WORKDIR /app
RUN pip install -r requirements.txt
ENV FLASK_APP app.py
ENTRYPOINT ["supervisord"]
CMD ["-c", "/app/supervisord.conf"]
