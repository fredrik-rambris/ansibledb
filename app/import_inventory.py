#!/usr/bin/env python

from flask import Flask
from models import *
import ansible.inventory.group
from ansible.inventory.ini import InventoryParser
import sys, os, re, yaml
from pprint import pprint

#aimport logging
#logger = logging.getLogger('peewee')
#logger.setLevel(logging.DEBUG)
#logger.addHandler(logging.StreamHandler())


app = Flask(__name__)
app.config.from_pyfile('settings.cfg')
database.init_app(app)

ungrouped = ansible.inventory.group.Group('ungrouped')
all = ansible.inventory.group.Group('all')
all.add_child_group(ungrouped)
groups = dict(all=all, ungrouped=ungrouped)
ip = InventoryParser(None, groups, filename=sys.argv[1])

db_groups={}

nogroups=['all', 'ungrouped']

for (groupname, group) in ip.groups.iteritems():
    if groupname in nogroups:
        continue
    db_group, created = Group.get_or_create(name=groupname)
    db_groups[groupname] = db_group
    if created:
        print("Created", db_group.name)

for (groupname, group) in ip.groups.iteritems():
    if groupname in nogroups:
        continue

    for parentgroup in group.parent_groups:
        if(parentgroup.name in nogroups):
            continue
        groupchild, created = GroupChild.get_or_create(parent=db_groups[parentgroup.name], child=db_groups[groupname])
        if created:
            print("Added", parentgroup.name, "as parent of", groupname)
    for childgroup in group.child_groups:
        if(childgroup.name in nogroups):
            continue
        groupchild, created = GroupChild.get_or_create(parent=db_groups[groupname], child=db_groups[childgroup.name])
        if created:
            print("Added", childgroup.name, "as child of", groupname)

    for k, v in group.vars.iteritems():
        groupvar, created = GroupVar.get_or_create(group=db_groups[groupname], name=k.trim(), defaults={'value': str(v).trim()})
        if created:
            print("Added var",k,"to group",groupname,"with value", str(v))
        else:
            groupvar.value = str(v).trim()
            groupvar.save()


for (hostname, host) in ip.hosts.iteritems():
    db_host, created = Host.get_or_create(hostname=hostname)
    if created:
        print("Created host", hostname)
    for hostgroup in host.groups:
        db_hostgroup, created = GroupHost.get_or_create(group=db_groups[hostgroup.name], host=db_host)
        if created:
            print("Added host",hostname,"to group",hostgroup.name)
    for k, v in host.vars.iteritems():
        hostvar, created = HostVar.get_or_create(host=db_host, name=k.trim(), defaults={'value': str(v).trim()})
        if created:
            print("Added var",k,"to host",hostname,"with value", str(v))
        else:
            hostvar.value = str(v).trim()
            hostvar.save()

def parse_vars(file_path):
    vars = {}
    with open(file_path) as f:

        var = None

        for x in f.readlines():
            m = re.match(r'^([^#]*)#(.*)$', x)
            if m:  # The line contains a hash / comment
                x = m.group(1)
            else:
                x = x.rstrip()

            if not x.strip():
                continue

            if x == '---':
                continue

            m = re.match(r'^([^: ]*)\s*:\s*(.*)$', x)

            if m:
                if var:
                    vars[var['k']] = var['v'].rstrip()
                    var = None
                if m.group(2):
                    vars[m.group(1)] = m.group(2)
                else:
                    var = {'k': m.group(1), 'v': ""}
            elif var:
                if 'indent' not in var:
                    var['indent'] = re.search('\S', x).start()

                var['v'] += x[var['indent']:] + "\n"

        if var:
            vars[var['k']] = var['v'].rstrip()
            var = None

    return vars

group_vars_dir=os.path.join(os.path.dirname(sys.argv[1]), 'group_vars')
if os.path.isdir(group_vars_dir):
    for filename in os.listdir(group_vars_dir):
        file_path = os.path.join(group_vars_dir, filename)
        groupname, ext = os.path.splitext(filename)
        if ext != '.yml':
            groupname = filename

        print groupname
        print "-" * len(groupname)

        if not groupname in db_groups:
            db_group, created = Group.get_or_create(name=groupname)
        else:
            db_group = db_groups[groupname]

        if os.path.isdir(file_path):
            if os.path.exists(os.path.join(file_path, 'main.yml')):
                file_path = os.path.join(file_path, 'main.yml')
            else:
                continue

        vars = parse_vars( file_path )

        for k, v in vars.iteritems():
            groupvar, created = GroupVar.get_or_create(group=db_group, name=k, defaults={'value': v})
            if created:
                print("Added var",k,"to group",groupname,"with value", v)
            else:
                groupvar.value = v
                groupvar.save()

        print
        print




host_vars_dir=os.path.join(os.path.dirname(sys.argv[1]), 'host_vars')
if os.path.isdir(host_vars_dir):
    for filename in os.listdir(host_vars_dir):
        file_path = os.path.join(host_vars_dir, filename)
        hostname, ext = os.path.splitext(filename)

        if ext != '.yml':
            hostname = filename

        print hostname
        print "-" * len(hostname)

        try:
            db_host = Host.select().where(Host.hostname == hostname).get()
        except Host.DoesNotExist:
            print("Does not exist")
            continue

        if os.path.isdir(file_path):
            if os.path.exists(os.path.join(file_path, 'main.yml')):
                file_path = os.path.join(file_path, 'main.yml')
            else:
                continue

        vars = parse_vars( file_path )

        pprint(vars)

        for k, v in vars.iteritems():
            hostvar, created = HostVar.get_or_create(host=db_host, name=k, defaults={'value': v})
            if created:
                print("Added var",k,"to host",hostname,"with value", v)
            else:
                hostvar.value = v
                hostvar.save()

        print
        print
