from peewee import *
from playhouse.flask_utils import FlaskDB
import re
import yaml
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

database = FlaskDB()

class UnknownField(object):
    def __init__(self, *_, **__): pass

class Group(database.Model):
    id = PrimaryKeyField()
    name = CharField(unique=True)
    comment = TextField()
    state = IntegerField(default=1)

    class Meta:
        db_table = 'groups'

    def __init__(self, *args, **kwargs):
        super(database.Model, self).__init__(*args, **kwargs)

        # Get hosts not linked to us
        self.nonlinked_hosts = ( Host.select()
            .join(GroupHost, JOIN.LEFT_OUTER)
            .where( (GroupHost.group != self) | (GroupHost.group.is_null()) )
            .group_by(Host.id)
            .order_by(Host.hostname) )

        # Manually overridden to 1. prefetch and 2. order by name
        self.hosts = GroupHost.select(GroupHost,Host).join(Host).order_by(Host.hostname).where(GroupHost.group == self)
        self.parents = GroupChild.select(GroupChild,Group).join(Group).order_by(Group.name).where(GroupChild.child == self)
        self.children = GroupChild.select(GroupChild,Group).join(Group).order_by(Group.name).where(GroupChild.parent == self)

        self.linked_group_ids = ( Group.select(Group.id)
            .join(GroupChild, on=( (GroupChild.parent_id == Group.id) | (GroupChild.child_id == Group.id) ) )
            .where( (GroupChild.parent == self) | (GroupChild.child == self) ) )

        # Get groups that is not already a child or parent to this group and not 'all'
        self.nonlinked_groups = Group.select().where(Group.id.not_in(self.linked_group_ids) & (Group.id != 1) & (Group.state < 3) ).order_by(Group.name)

    def get_all_parents(self):
        '''Get a list of parents with each level sorted and prepended. So top level groups first'''
        imediate_parents=[]
        all_parents=[]

        level = 0

        for groupchild in self.parents.where(Group.state < 3):
            groupchild.parent.level = level
            imediate_parents.append(groupchild.parent)

        while imediate_parents:
            current_parents=[]
            level += 1
            for group in imediate_parents:
                for groupchild in group.parents.where(Group.state < 3):
                    if groupchild.parent in all_parents:
                        raise AttributeError('Circular parent groups')
                    groupchild.parent.level = level
                    current_parents.append(groupchild.parent)

                all_parents.insert(0, group)

            imediate_parents = current_parents

        return all_parents

    def get_effective_vars(self):
        '''Returns a list of tuples with vars and the group they came from.
           Vars from lower groups overrides vars from above.'''
        vars = {}
        for parent in self.get_all_parents():
            for var in parent.vars.where(GroupVar.state < 3).order_by(GroupVar.name):
                vars[var.name] = (var, parent)

        for var in self.vars.where(GroupVar.state < 3).order_by(GroupVar.name):
            vars[var.name] = (var, self)

        ordered_vars = OrderedDict()
        for k in sorted(vars.iterkeys()):
            ordered_vars[k] = vars[k]

        return ordered_vars


    @staticmethod
    def get_top():
        return Group.select().join(GroupChild, JOIN.LEFT_OUTER).where( (GroupChild.child == None) & (Group.state < 3) ).order_by(Group.name != 'all', Group.name)

class GroupChild(database.Model):
    '''Connecting groups to other groups'''
    child = ForeignKeyField(db_column='child_id', rel_model=Group, to_field='id', related_name='parents')
    parent = ForeignKeyField(db_column='parent_id', rel_model=Group, related_name='children', to_field='id')

    class Meta:
        db_table = 'group_children'
        indexes = (
            (('parent', 'child'), True),
        )
        primary_key = CompositeKey('child', 'parent')


class Host(database.Model):
    hostname = CharField(unique=True)
    id = PrimaryKeyField()
    comment = TextField()
    state = IntegerField(default=1)

    def __init__(self, *args, **kwargs):
        super(database.Model, self).__init__(*args, **kwargs)

        self.vars = HostVar.select().where(HostVar.host == self).order_by(HostVar.name)
        self.groups = ( GroupHost.select(GroupHost, Group)
            .join(Group)
            .where(GroupHost.host == self)
            .order_by(Group.name)
            .group_by(Group.id) )

        self.linked_group_ids = ( Group.select(Group.id)
            .join(GroupHost)
            .where(GroupHost.host == self)
            .order_by(Group.name)
            .group_by(Group.id) )

        self.nonlinked_groups = Group.select().where(Group.id.not_in(self.linked_group_ids) & (Group.id != 1) & (Group.state < 3) ).order_by(Group.name)

    @staticmethod
    def get_ungrouped():
        return ( Host.select()
        .join(GroupHost, JOIN.LEFT_OUTER)
        .where( (GroupHost.group.is_null()) & (Host.state < 3) )
        .order_by(Host.hostname) )

    def get_effective_vars(self):
        vars = {}
        for hostgroup in self.groups.where(Group.state < 3):
            for k,v in hostgroup.group.get_effective_vars().iteritems():
                vars[k] = v

        for var in self.vars.where(HostVar.state < 3).order_by(HostVar.name):
            vars[var.name] = (var, self)

        ordered_vars = OrderedDict()
        for k in sorted(vars.iterkeys()):
            ordered_vars[k] = vars[k]

        return ordered_vars


    class Meta:
        db_table = 'hosts'


class GroupHost(database.Model):
    '''Connecting hosts to group'''
    group = ForeignKeyField(db_column='group_id', rel_model=Group, to_field='id', related_name='hosts')
    host = ForeignKeyField(db_column='host_id', rel_model=Host, to_field='id', related_name='groups')

    class Meta:
        db_table = 'group_hosts'
        indexes = (
            (('group', 'host'), True),
        )
        primary_key = CompositeKey('group', 'host')


class GroupVar(database.Model):
    group = ForeignKeyField(db_column='group_id', rel_model=Group, to_field='id', related_name='vars')
    name = CharField()
    value = TextField(null=True)
    comment = TextField()
    state = IntegerField(default=1)

    def isyaml(self):
        return type(yaml.load(self.value)) is list or type(yaml.load(self.value)) is dict

    def rows(self):
        return len(re.split('\r\n|\r|\n', self.value))

    class Meta:
        db_table = 'group_vars'
        indexes = (
            (('group', 'name'), True),
        )
        primary_key = CompositeKey('group', 'name')


class HostVar(database.Model):
    host = ForeignKeyField(db_column='host_id', rel_model=Host, to_field='id', related_name='vars')
    name = CharField()
    value = TextField(null=True)
    comment = TextField()
    state = IntegerField(default=1)

    def isyaml(self):
        return type(yaml.load(self.value)) is list or type(yaml.load(self.value)) is dict

    def rows(self):
        return len(re.split('\r\n|\r|\n', self.value))

    class Meta:
        db_table = 'host_vars'
        indexes = (
            (('host', 'name'), True),
        )
        primary_key = CompositeKey('host', 'name')


def get_inventory():
    '''Generate inventory structure for Ansible. Ready to be JSON encoded'''

    inventory = OrderedDict()

    for group in Group.select().where( (Group.name != 'all') & (Group.state == 1) ).order_by(Group.name):
        obj = OrderedDict({'hosts': [], 'vars': OrderedDict(), 'children': []})


        for grouphost in group.hosts.where(Host.state == 1):
            obj['hosts'].append(grouphost.host.hostname)

        for groupchild in group.children.where(Group.state == 1):
            obj['children'].append(groupchild.child.name)

        for groupvar in group.vars.where(GroupVar.state ==1):
            obj['vars'][groupvar.name] = yaml.load(groupvar.value)

        inventory[group.name] = obj

    hostvars = OrderedDict()
    inventory['_meta'] = { 'hostvars': hostvars }
    
    for hostvar in HostVar.select(HostVar, Host).join(Host).where( (Host.state == 1) & (HostVar.state == 1) ).order_by(Host.hostname, HostVar.name):
        try:
            hostvars[hostvar.host.hostname][hostvar.name] = yaml.load(hostvar.value)
        except KeyError:
            hostvars[hostvar.host.hostname] = OrderedDict({hostvar.name: hostvar.value})

    return inventory



class Version(database.Model):
    based_on = ForeignKeyField(db_column='based_on', null=True, rel_model='self', to_field='id')
    closed = IntegerField()
    comment = TextField(null=True)
    current = CharField(null=True, unique=True)

    def set_current(self):
        Version.update(current=None).execute()
        self.current = '1'
        self.save()

    @staticmethod
    def get_current():
        return Version.get(Version.current == '1')

    class Meta:
        db_table = 'versions'

class VGroupChild(database.Model):
    child = ForeignKeyField(db_column='child_id', rel_model=Group, related_name='v_parents', to_field='id')
    parent = ForeignKeyField(db_column='parent_id', rel_model=Group, related_name='v_children', to_field='id')
    state = IntegerField(default=1)
    version = ForeignKeyField(db_column='version_id', rel_model=Version, to_field='id')

    class Meta:
        db_table = 'v_group_children'
        indexes = (
            (('version', 'parent', 'child'), True),
        )
        primary_key = CompositeKey('child', 'parent', 'version')

class VGroupVar(database.Model):
    comment = TextField(null=True)
    group = ForeignKeyField(db_column='group_id', rel_model=Group, to_field='id')
    name = CharField()
    state = IntegerField(default=1)
    value = TextField(null=True)
    version = ForeignKeyField(db_column='version_id', rel_model=Version, to_field='id')

    class Meta:
        db_table = 'v_group_vars'
        indexes = (
            (('version', 'group', 'name'), True),
        )
        primary_key = CompositeKey('group', 'name', 'version')

    @classmethod
    def create(cls, **query):
        if not 'value' in query:          
            try:
                var = GroupVar.get(group=query['group'], name=query['name'])
                query['value'] = var.value
            except GroupVarDoesNotExist:
                pass

            if 'state' not in query:
                query['state'] = 1

        return super(database.Model, cls).create(**query)


class VGroup(database.Model):
    comment = TextField(null=True)
    group = ForeignKeyField(db_column='id', rel_model=Group, to_field='id', related_name='versions')
    name = CharField()
    state = IntegerField(default=1)
    version = ForeignKeyField(db_column='version_id', rel_model=Version, to_field='id')

    class Meta:
        db_table = 'v_groups'
        indexes = (
            (('version', 'id'), True),
            (('version', 'name'), True),
        )
        primary_key = CompositeKey('id', 'version')

    @classmethod
    def create(cls, **query):
        if query['name'] == 'all':
            raise AttributeError("Cannot change 'all' group")
        defaults = query.copy()
        defaults['state'] = 4
        group, created = Group.get_or_create(name = query['name'], defaults=defaults)
        vgroup = {
            'name': group.name,
            'comment': group.comment,
            'group': group,
            'version': query['version']
        }
        return super(database.Model, cls).create(**vgroup)


class VHostVar(database.Model):
    comment = TextField(null=True)
    host = ForeignKeyField(db_column='host_id', rel_model=Host, to_field='id')
    name = CharField()
    state = IntegerField(default=1)
    value = TextField(null=True)
    version = ForeignKeyField(db_column='version_id', rel_model=Version, to_field='id')

    class Meta:
        db_table = 'v_host_vars'
        indexes = (
            (('version', 'host', 'name'), True),
        )
        primary_key = CompositeKey('host', 'name', 'version')


class VHost(database.Model):
    comment = TextField(null=True)
    hostname = CharField()
    host = ForeignKeyField(db_column='id', rel_model=Host, to_field='id', related_name='versions')
    state = IntegerField(default=1)
    version = ForeignKeyField(db_column='version_id', rel_model=Version, to_field='id')

    class Meta:
        db_table = 'v_hosts'
        indexes = (
            (('version', 'hostname'), True),
            (('version', 'id'), True),
        )
        primary_key = CompositeKey('id', 'version')

    @classmethod
    def create(cls, **query):
        defaults = query.copy()
        defaults['state'] = 4
        host, created = Host.get_or_create(hostname = query['hostname'], defaults=defaults)
        vhost = {
            'hostname': host.hostname,
            'comment': host.comment,
            'host': host,
            'version': query['version']
        }
        return super(database.Model, cls).create(**vhost)
