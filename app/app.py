#!/usr/bin/env python

from flask import Flask, request, redirect, url_for, make_response, render_template, abort, session, g
from urlparse import urlparse, urljoin
from models import *
import json

app = Flask(__name__)
app.config.update(
    truncate_lines = 10
)
app.config.from_pyfile('settings.cfg')
app.jinja_env.lstrip_blocks = True
app.jinja_env.trim_blocks = True
database.init_app(app)

if 'DATABASE_DEBUG' in app.config and app.config['DATABASE_DEBUG']:
    import logging
    logger = logging.getLogger('peewee')
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

@app.route('/')
def index():
    top_groups = Group.get_top()
    ungrouped_hosts = Host.get_ungrouped()
    return render_template('index.html.j2', top_groups=top_groups, ungrouped_hosts=ungrouped_hosts)

@app.route('/version/', methods=['GET'])
def versions():
    if 'switch_to' in request.args:
        try:
            version = Version.get(Version.id == request.args['switch_to'])
            session['version'] = version.id
            return redirect_back(url_for('index'))
        except Version.DoesNotExist:
            abort(404, 'Version does not exist')

    versions = Version.select()
    return render_template('versions.html.j2', versions=versions)


@app.route('/version/<int:id>', methods=['GET', 'POST'])
def version(id):
    version = Version.get(Version.id == id)
    if request.method == 'POST':
        version.comment = request.form['comment']
        version.save()
        return redirect(url_for('version', id=version.id))

    return render_template('version.html.j2', version=version)
 
@app.route('/version/fork/<int:id>')
def new_version(id):
    old_version = Version.get(Version.id == id)
    if not old_version.current:
        abort(400, 'Version is not current. Can only fork current version')

    version = Version.create(based_on = old_version)
    return redirect(url_for('version', id=version.id))
 
@app.route('/version/<int:version_id>/group/<name>', methods=['GET'])
def get_group(version_id, name):
    group = None
    try:
        group = VGroup.select().where( (VGroup.name == name) & (VGroup.version_id == version_id) ).get()
    except VGroup.DoesNotExist:
        pass
    if group != None and group.state > 2:
        abort(404, "Group does not exist")

    elif group == None:
        try:
            group = Group.select().where( (Group.name == name) & (Group.state < 3) ).get()
        except Group.DoesNotExist:
            abort(404, "Group does not exist")

    return render_template('group.html.j2', group=group)

@app.route('/group/', methods=['GET', 'POST'])
@app.route('/group/<name>', methods=['GET', 'POST'])
def group(name=None):
    try:
        if name != None:
            name = name.strip()
            group = Group.select().where( (Group.name == name) & (Group.state < 3) ).get()
        else:
            group = Group()

        if request.method == 'GET':
            if name != None and 'delete' in request.args:
                group.state = 3
                group.save()
                return redirect(url_for('index'))
        else:
            if 'groupname' in request.form:
                group.name = request.form['groupname'].strip()
                group.comment = request.form['comment'].strip()
                group.state = 1 if request.form['state'] == 'enabled' else 2
                group.save()
                return redirect(url_for('group', name=group.name))

        return render_template('group.html.j2', group=group)
    except Group.DoesNotExist:
        abort(404, "Group does not exist")


    return "Welcome to the darkside, we have cookies"


@app.route('/group/<groupname>/host/', methods=['POST'])
@app.route('/group/<groupname>/host/<hostname>', methods=['GET', 'DELETE'])
def grouphost(groupname, hostname=None):
    try:
        group = Group.select().where( (Group.name == groupname) & (Group.state < 3) ).get()
        if hostname != None:
            host = Host.select().where( (Host.hostname == hostname) & (Host.state <3 ) ).get()
        elif 'hostname' in request.form:
            host = Host.select().where( (Host.hostname == request.form['hostname']) & (Host.state < 3) ).get()
        else:
            abort(404, "Host does not exist")

        if request.method == 'DELETE' or 'delete' in request.args:
            GroupHost.delete().where( (GroupHost.group == group) & (GroupHost.host == host) ).execute()
        elif request.method == 'POST':
            try:
                GroupHost.create(host=host, group=group)
            except IntegrityError:
                pass

        return redirect(url_for('group', name=group.name))
    except Group.DoesNotExist:
        abort(404, "Group does not exist")
    except Host.DoesNotExist:
        abort(404, "Host does not exist")

    return "Welcome to the darkside, we have cookies"

@app.route('/group/<childname>/parent/', methods=['POST'], defaults={'return_to': 'child'})
@app.route('/group/<childname>/parent/<parentname>', methods=['GET', 'DELETE'], defaults={'return_to': 'child'})
@app.route('/group/<parentname>/child/', methods=['POST'], defaults={'return_to': 'parent'})
@app.route('/group/<parentname>/child/<childname>', methods=['GET', 'DELETE'], defaults={'return_to': 'parent'})
def groupchild(parentname=None, childname=None, return_to='parent'):
    try:
        if parentname != None:
            parent = Group.select().where( (Group.name == parentname) & (Group.state < 3) ).get()
        elif 'parent' in request.form:
            parent = Group.select().where(Group.name == request.form['parent']).get()
        else:
            abort(404, "Group does not exist")

        if childname != None:
            child = Group.select().where( (Group.name == childname) & (Group.state < 3) ).get()
        elif 'child' in request.form:
            child = Group.select().where( (Group.name == request.form['child']) & (Group.state < 3) ).get()
        else:
            abort(404, "Group does not exist")

        if request.method == 'DELETE' or 'delete' in request.args:
            GroupChild.delete().where(GroupChild.parent == parent).where(GroupChild.child == child).execute()
        elif request.method == 'POST':
            try:
                GroupChild.create(parent=parent, child=child)
            except IntegrityError:
                pass

        if return_to == 'parent':
            return redirect(url_for('group', name=parent.name))
        elif return_to == 'child':
            return redirect(url_for('group', name=child.name))

    except Group.DoesNotExist:
        abort(404, "Group does not exist")
    except Host.DoesNotExist:
        abort(404, "Host does not exist")

    return "Welcome to the darkside, we have cookies"



@app.route('/group/<groupname>/var/', methods=['GET', 'POST', 'PUT'])
@app.route('/group/<groupname>/var/<name>', methods=['GET', 'POST', 'PUT'])
def groupvar(groupname, name=None):
    try:
        from_group = None
        group = Group.select().where( (Group.name == groupname) & (Group.state < 3) ).get()
        if name != None:
            if 'from_group' in request.args:
                from_group = Group.select().where( (Group.name == request.args['from_group']) & (Group.state < 3) ).get()
                from_var = from_group.vars.where( (GroupVar.name == name) & (GroupVar.state < 3) ).get()
                var = GroupVar(group=group, name=from_var.name, value=from_var.value)
            else:
                var = group.vars.where( (GroupVar.name == name) & (GroupVar.state < 3) ).get()

            if 'delete' in request.args:
                var.state = 3
                var.save()
                return redirect(url_for('group', name=group.name))
        else:
            var = GroupVar(group=group, name="", value="")

        if request.method == 'GET':
            return render_template('groupvar.html.j2', group=group, var=var, from_group=from_group)
        else:
            force_insert=False

            if var.name != request.form['varname']:
                var.delete_instance()
                force_insert=True

            var.name = request.form['varname'].strip()
            var.value = request.form['varval'].strip()
            var.comment = request.form['comment'].strip()
            var.state = 1 if request.form['state'] == 'enabled' else 2

            var.save(force_insert=force_insert)
            return redirect(url_for('group', name=group.name))

    except Group.DoesNotExist:
        abort(404, "Group does not exist")
    except GroupVar.DoesNotExist:
        abort(404, "Groupvar does not exist")

    return "Welcome to the darkside, we have cookies"



@app.route('/host/', methods=['GET', 'POST', 'PUT'])
@app.route('/host/<name>', methods=['GET', 'POST', 'PUT'])
def host(name=None):
    try:
        if name != None:
            host = Host.select().where( (Host.hostname == name) & (Host.state < 3) ).get()
        else:
            host = Host()

        if 'group' in request.args:
            group = Group.select().where( (Group.name == request.args['group']) & (Group.state < 3) ).get()
        elif 'group' in request.form:
            group = Group.select().where( (Group.name == request.form['group']) & (Group.state < 3) ).get()
        else:
            group = None

        if request.method == 'GET':
            if name != None and 'delete' in request.args:
                if group!=None:
                    GroupHost.delete().where( (GroupHost.group == group) & (GroupHost.host == host) ).execute()
                    return redirect(url_for('host', name=host.hostname))
                else:
                    host.state = 3
                    host.save()
                    return redirect(url_for('index'))

            return render_template('host.html.j2', host=host, group=group)
        else:
            if 'hostname' in request.form:
                host.hostname = request.form['hostname'].strip()
                host.comment = request.form['comment'].strip()
                host.state = 1 if request.form['state'] == 'enabled' else 2
                host.save()

            if group != None and host.id != None:
                try:
                    GroupHost.create(host=host, group=group)
                except IntegrityError:
                    pass
                if 'group' in request.form:
                    return redirect(url_for('host', name=host.hostname))
                else:
                    return redirect(url_for('group', name=group.name))
            else:
                return redirect(url_for('host', name=host.hostname))

    except Host.DoesNotExist:
        abort(404, "Host does not exist")
    except Group.DoesNotExist:
        abort(404, "Group does not exist")

    return "Welcome to the darkside, we have cookies"



@app.route('/host/<hostname>/var/', methods=['GET', 'POST', 'PUT'])
@app.route('/host/<hostname>/var/<name>', methods=['GET', 'POST', 'PUT'])
def hostvar(hostname, name=None):
    try:
        from_group = None
        host = Host.select().where( (Host.hostname == hostname) & (Host.state < 3) ).get()
        if name != None:

            if 'from_group' in request.args:
                from_group = Group.select().where( (Group.name == request.args['from_group']) & (Group.state < 3) ).get()
                from_var = from_group.vars.where( (GroupVar.name == name) & (GroupVar.state < 3) ).get()
                var = HostVar(host=host, name=from_var.name, value=from_var.value)
            else:
                var = host.vars.where( (HostVar.name == name) & (HostVar.state < 3) ).get()
            if 'delete' in request.args:
                var.state = 3
                var.save()
                return redirect(url_for('host', name=host.hostname))
        else:
            var = HostVar(host=host, name="", value="")

        if request.method == 'GET':
            return render_template('hostvar.html.j2', host=host, var=var, from_group=from_group)
        else:
            force_insert=False

            if var.name != request.form['varname']:
                var.delete_instance()
                force_insert=True

            var.name = request.form['varname'].strip()
            var.value = request.form['varval'].strip()
            var.comment = request.form['comment'].strip()
            var.state = 1 if request.form['state'] == 'enabled' else 2

            var.save(force_insert=force_insert)
            return redirect(url_for('host', name=host.hostname))

    except Host.DoesNotExist:
        abort(404, "Host does not exist")
    except HostVar.DoesNotExist:
        abort(404, "Hostvar does not exist")

    return "Welcome to the darkside, we have cookies"

@app.route('/s', methods=['GET'])
def search():
    groups = Group.select().where( (Group.name ** ('%' + request.args['q'] + '%')) & (Group.state < 3) ).order_by(Group.name)
    group_vars = GroupVar.select(GroupVar, Group).join(Group).where( (GroupVar.name ** ('%' + request.args['q'] + '%')) & (Group.state < 3) & (GroupVar.state < 3) ).order_by(GroupVar.name)

    hosts = Host.select().where( (Host.hostname ** ('%' + request.args['q'] + '%')) & (Host.state < 3) ).order_by(Host.hostname)
    host_vars = HostVar.select(HostVar, Host).join(Host).where( (HostVar.name ** ('%' + request.args['q'] + '%')) & (Host.state < 3) & (HostVar.state < 3) ).order_by(HostVar.name)

    return render_template('search.html.j2',
            groups = groups,
            group_vars = group_vars,
            hosts = hosts,
            host_vars = host_vars )

@app.route('/inventory.json', methods=['GET'])
def inventory_json():
    jsonargs={}
    if 'pretty' in request.args:
        jsonargs['indent']=2
    resp = make_response(json.dumps(get_inventory(), **jsonargs))
    resp.headers['Content-Type'] = 'application/json; charset=utf-8'
    return resp

@app.before_request
def before_request():
    if 'version' not in session:
        g.version = Version.get_current()
        session['version'] = g.version.id
    else:
        g.version = Version.get(Version.id == session['version'])

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def redirect_back(fallback):
    if is_safe_url(request.referrer):
        return redirect(request.referrer)
    else:
        return redirect(fallback)

@app.template_filter()
def truncate_lines(value):
    nl = app.config['truncate_lines']
    rows = value.split("\n")
    ret = "\n".join(rows[:nl])
    if len(rows) > nl:
        ret += "\n<em>... %d more lines ...</em>" % ( len(rows) - nl )
    return ret

if __name__ == '__main__':
    print(json.dumps(get_inventory(), indent=2))
