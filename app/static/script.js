$(document).ready(function() {
	jQuery.each(jQuery('textarea[data-autoresize]'), function() {
		var offset = this.offsetHeight - this.clientHeight;
		
		var resizeTextarea = function(el) {
			el.rows = el.value.split(/\r\n|\r|\n/).length;
			jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
		};
		jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
		//resizeTextarea(this);
	});
});
