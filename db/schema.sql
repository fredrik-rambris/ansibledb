SET foreign_key_checks = 0;

### BEGIN Basic schema ###

DROP TABLE IF EXISTS `states`;
CREATE TABLE `states` (
	`id` INT UNSIGNED NOT NULL PRIMARY KEY,
	`name` VARCHAR(10) NOT NULL,
	UNIQUE(`name`)
) ENGINE=InnoDB;

INSERT INTO `states` (`id`, `name`) VALUES
	(1, 'enabled'),
	(2, 'disabled'),
	(3, 'deleted'),
	(4, 'uninitialized');

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
	`id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`name` VARCHAR(200) NOT NULL,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	UNIQUE(`name`)
) ENGINE=InnoDB;

INSERT INTO `groups` (`id`, `name`) VALUES (1, "all");

DROP TABLE IF EXISTS `group_children`;
CREATE TABLE `group_children` (
	`parent_id` INT UNSIGNED NOT NULL,
	`child_id` INT UNSIGNED NOT NULL,
	PRIMARY KEY(`parent_id`, `child_id`),	
	FOREIGN KEY (`parent_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`child_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `group_vars`;
CREATE TABLE `group_vars` (
	`group_id` INT UNSIGNED NOT NULL,
	`name` VARCHAR(200) NOT NULL,
	`value` MEDIUMTEXT,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	PRIMARY KEY(`group_id`, `name`),
	FOREIGN KEY (`group_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `hosts`;
CREATE TABLE `hosts` (
	`id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`hostname` VARCHAR(200) NOT NULL,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	UNIQUE(`hostname`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `host_vars`;
CREATE TABLE `host_vars` (
	`host_id` INT UNSIGNED NOT NULL,
	`name` VARCHAR(200) NOT NULL,
	`value` MEDIUMTEXT,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	PRIMARY KEY(`host_id`, `name`),
	FOREIGN KEY (`host_id`) REFERENCES `hosts`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `group_hosts`;
CREATE TABLE `group_hosts` (
	`group_id` INT UNSIGNED NOT NULL,
	`host_id` INT UNSIGNED NOT NULL,
	PRIMARY KEY(`group_id`, `host_id`),	
	FOREIGN KEY (`group_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`host_id`) REFERENCES `hosts`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB;

### END Basic schema ###

### BEGIN Versioning schema ###

DROP TABLE IF EXISTS `versions`;
CREATE TABLE `versions` (
	`id` INT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`based_on` INT UNSIGNED DEFAULT NULL,
	`comment` TEXT DEFAULT NULL,
	`closed` INT UNSIGNED NOT NULL DEFAULT 0,
	`current` ENUM('1') DEFAULT NULL,
	UNIQUE(`current`),
	FOREIGN KEY (`based_on`) REFERENCES `versions`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB;

INSERT INTO `versions` (`id`, `comment`, `current`, `closed`) VALUES (1, 'First base', '1', 1);


DROP TABLE IF EXISTS `v_groups`;
CREATE TABLE `v_groups` (
	`id` INT UNSIGNED NOT NULL,
	`version_id` INT UNSIGNED NOT NULL,
	`name` VARCHAR(200) NOT NULL,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	FOREIGN KEY (`id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`version_id`) REFERENCES `versions`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	PRIMARY KEY(`version_id`, `id`),
	UNIQUE(`version_id`, `name`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `v_group_children`;
CREATE TABLE `v_group_children` (
	`version_id` INT UNSIGNED NOT NULL,
	`parent_id` INT UNSIGNED NOT NULL,
	`child_id` INT UNSIGNED NOT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	PRIMARY KEY(`version_id`, `parent_id`, `child_id`),	
	FOREIGN KEY (`version_id`) REFERENCES `versions`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	FOREIGN KEY (`parent_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`child_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `v_group_vars`;
CREATE TABLE `v_group_vars` (
	`version_id` INT UNSIGNED NOT NULL,
	`group_id` INT UNSIGNED NOT NULL,
	`name` VARCHAR(200) NOT NULL,
	`value` MEDIUMTEXT,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	PRIMARY KEY(`version_id`, `group_id`, `name`),
	FOREIGN KEY (`version_id`) REFERENCES `versions`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`group_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `v_hosts`;
CREATE TABLE `v_hosts` (
	`id` INT UNSIGNED NOT NULL,
	`version_id` INT UNSIGNED NOT NULL,
	`hostname` VARCHAR(200) NOT NULL,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	FOREIGN KEY (`id`) REFERENCES `hosts`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`version_id`) REFERENCES `versions`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
	PRIMARY KEY(`version_id`, `id`),
	UNIQUE(`version_id`, `hostname`)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `v_host_vars`;
CREATE TABLE `v_host_vars` (
	`version_id` INT UNSIGNED NOT NULL,
	`host_id` INT UNSIGNED NOT NULL,
	`name` VARCHAR(200) NOT NULL,
	`value` MEDIUMTEXT,
	`comment` TEXT DEFAULT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	PRIMARY KEY(`version_id`, `host_id`, `name`),
	FOREIGN KEY (`version_id`) REFERENCES `versions`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`host_id`) REFERENCES `hosts`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB;

DROP TABLE IF EXISTS `v_group_hosts`;
CREATE TABLE `v_group_hosts` (
	`version_id` INT UNSIGNED NOT NULL,
	`group_id` INT UNSIGNED NOT NULL,
	`host_id` INT UNSIGNED NOT NULL,
	`state` INT UNSIGNED NOT NULL DEFAULT 1,
	PRIMARY KEY(`version_id`, `group_id`, `host_id`),	
	FOREIGN KEY (`version_id`) REFERENCES `versions`(`id`) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY (`group_id`) REFERENCES `groups`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`host_id`) REFERENCES `hosts`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
	FOREIGN KEY (`state`) REFERENCES `states`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB;


# Begin a change by creating a version
# INSERT INTO versions (comment) VALUES ('A new beginning');
# SELECT LAST_INSERT_ID INTO @version_id;
#
# If creating a new object, create it in the main table but with state = uninitialized
# INSERT INTO groups (name, comment, state) VALUES ('a_new_group', 'Stuff to be said', 4);
# SELECT LAST_INSERT_ID INTO @group_id;
#
# Create a new copy in the v_ prefixed tables
# INSERT INTO v_groups ( id, version_id, name, comment, state ) SELECT id, @version_id, name, comment, 1 FROM groups WHERE id = @group_id;
#
# Transisions from state 4 to 1 or 2 should be displayed as NEW
# A state has been added to link tables v_group_children and v_group_hosts to allow them to be deleted.
#
# When merging a version to main a JSON representation of the change (old, new) should be logged to an audit table, the contents 
# should be copied into its main counter parts and the version should be marked closed. All prior verions could be deleted.
# The versions.current should be nulled on old version and set to '1' on the new version.
#
# When an object is to be deleted it can merely be set to state = 3. Only when all versions referencing it has been merged/removed can the
# objects be purged.
#
# In reality state = 3 should be considered deleted for good.


### END Versioning schema ###


SET foreign_key_checks = 1;

# Functions not actually used in application but nice to have

DELIMITER $$

DROP PROCEDURE IF EXISTS `GET_GROUP`$$
CREATE PROCEDURE `GET_GROUP` (IN in_groupname VARCHAR(200), IN in_version_id INT UNSIGNED)
BEGIN
	SELECT id FROM versions WHERE current INTO @current_version_id;
	IF in_version_id = @current_version_id OR  in_version_id IS NULL THEN
		SELECT g.id, @current_version_id AS version_id, g.name, g.comment, g.state FROM groups AS g WHERE g.name=in_groupname AND g.state<3;
	ELSE
		SELECT g.id, IF(vg.id IS NOT NULL, vg.version_id, @current_version_id) AS version_id, IF(vg.id IS NOT NULL, vg.name, g.name) AS name, IF(vg.id IS NOT NULL, vg.comment, g.comment) AS comment, IF(vg.id IS NOT NULL, vg.state, g.state) AS state FROM groups AS g LEFT JOIN v_groups AS vg ON vg.id=g.id WHERE (vg.name=in_groupname AND vg.version_id=in_version_id AND vg.state<3) OR (vg.name IS NULL AND g.name=in_groupname AND g.state<3);
	END IF;
END$$

DROP PROCEDURE IF EXISTS `GET_GROUP_PARENTS`$$
CREATE PROCEDURE `GET_GROUP_PARENTS` (IN in_group_id INT UNSIGNED, IN in_version_id INT UNSIGNED)
BEGIN
	SELECT id FROM versions WHERE current INTO @current_version_id;
	IF in_version_id = @current_version_id THEN
		SELECT g.id, NULL AS version_id, g.name, g.comment, g.state FROM groups AS g WHERE g.name=in_groupname AND g.state<3;
	ELSE

		SELECT g.id, IF(vg.id IS NOT NULL, vg.version_id, NULL) AS version_id, IF(vg.id IS NOT NULL, vg.name, g.name) AS name, IF(vg.id IS NOT NULL, vg.comment, g.comment) AS comment, IF(vg.id IS NOT NULL, vg.state, g.state) AS state FROM groups AS g LEFT JOIN v_groups AS vg ON vg.id=g.id WHERE (vg.name=in_groupname AND vg.version_id=in_version_id AND vg.state<3) OR (vg.name IS NULL AND g.name=in_groupname AND g.state<3);
	END IF;
END$$


DROP PROCEDURE IF EXISTS `SET_HOST_VAR`$$
CREATE PROCEDURE `SET_HOST_VAR` (IN in_hostname VARCHAR(200), IN in_varname VARCHAR(200), IN in_varval MEDIUMTEXT)
jproc:BEGIN
	SET @host_id=NULL;
	SELECT id FROM hosts WHERE hostname=in_hostname INTO @host_id;
	IF @host_id IS NULL THEN
		SELECT NULL AS id, "Host not found" AS error;
		LEAVE jproc;
	END IF;
	INSERT INTO host_vars (host_id, name, value) VALUES (@host_id, `in_varname`, `in_varval`)
	ON DUPLICATE KEY UPDATE `value`=VALUES(`value`);
	SELECT id FROM hosts WHERE hosts.`hostname`=`in_hostname`;
END$$

DROP PROCEDURE IF EXISTS `SET_GROUP_VAR`$$
CREATE PROCEDURE `SET_GROUP_VAR` (IN in_groupname VARCHAR(200), IN in_varname VARCHAR(200), IN in_varval MEDIUMTEXT)
jproc:BEGIN
	SET @group_id=NULL;
	SELECT id FROM groups WHERE name=in_groupname INTO @group_id;
	IF @group_id IS NULL THEN
		SELECT NULL AS id, "Group not found" AS error;
		LEAVE jproc;
	END IF;
	INSERT INTO group_vars (group_id, name, value) VALUES (@group_id, `in_varname`, `in_varval`)
	ON DUPLICATE KEY UPDATE `value`=VALUES(`value`);
	SELECT id FROM groups WHERE groups.`name`=`in_groupname`;
END$$

DROP PROCEDURE IF EXISTS `ADD_GROUP_TO_GROUP`$$
CREATE PROCEDURE `ADD_GROUP_TO_GROUP` (IN in_parentname VARCHAR(200), IN in_childname VARCHAR(200))
jproc:BEGIN
	SET @parent_id=NULL;
	SET @child_id=NULL;

	SELECT id FROM groups WHERE name=in_parentname INTO @parent_id;
	SELECT id FROM groups WHERE name=in_childname INTO @child_id;

	IF @parent_id IS NULL OR @child_id IS NULL THEN
		SELECT @parent_id AS parent_id, @child_id AS child_id, "Groups not found" AS error;
		LEAVE jproc;
	END IF;

	INSERT INTO group_children (parent_id, child_id) VALUES (@parent_id, @child_id)
	ON DUPLICATE KEY UPDATE `child_id`=VALUES(`child_id`);
	SELECT @group_id AS group_id, @host_id AS host_id;
END$$

DROP PROCEDURE IF EXISTS `REMOVE_GROUP_FROM_GROUP`$$
CREATE PROCEDURE `REMOVE_GROUP_FROM_GROUP` (IN in_parentname VARCHAR(200), IN in_childname VARCHAR(200))
jproc:BEGIN
	SET @parent_id=NULL;
	SET @child_id=NULL;

	SELECT id FROM groups WHERE name=in_parentname INTO @parent_id;
	SELECT id FROM groups WHERE name=in_childname INTO @child_id;

	IF @parent_id IS NULL OR @child_id IS NULL THEN
		SELECT @parent_id AS parent_id, @child_id AS child_id, "Groups not found" AS error;
		LEAVE jproc;
	END IF;

	DELETE FROM group_children WHERE parent_id=@parent_id AND child_id=@child_id LIMIT 1;
	SELECT @group_id AS group_id, @host_id AS host_id;
END$$

DROP PROCEDURE IF EXISTS `ADD_HOST_TO_GROUP`$$
CREATE PROCEDURE `ADD_HOST_TO_GROUP` (IN in_groupname VARCHAR(200), IN in_hostname VARCHAR(200))
jproc:BEGIN
	SET @group_id=NULL;
	SELECT id FROM groups WHERE name=in_groupname INTO @group_id;
	IF @group_id IS NULL THEN
		SELECT NULL AS group_id, NULL AS host_id, "Group not found" AS error;
		LEAVE jproc;
	END IF;
	SET @host_id=NULL;
	SELECT id FROM hosts WHERE hostname=in_hostname INTO @host_id;
	IF @host_id IS NULL THEN
		SELECT NULL AS group_id, NULL AS host_id, "Host not found" AS error;
		LEAVE jproc;
	END IF;
	INSERT INTO group_hosts (group_id, host_id) VALUES (@group_id, @host_id)
	ON DUPLICATE KEY UPDATE `host_id`=VALUES(`host_id`);
	SELECT @group_id AS group_id, @host_id AS host_id;
END$$

DROP PROCEDURE IF EXISTS `REMOVE_HOST_FROM_GROUP`$$
CREATE PROCEDURE `REMOVE_HOST_FROM_GROUP` (IN in_groupname VARCHAR(200), IN in_hostname VARCHAR(200))
jproc:BEGIN
	SET @group_id=NULL;
	SELECT id FROM groups WHERE name=in_groupname INTO @group_id;
	IF @group_id IS NULL THEN
		SELECT NULL AS group_id, NULL AS host_id, "Group not found" AS error;
		LEAVE jproc;
	END IF;
	SET @host_id=NULL;
	SELECT id FROM hosts WHERE hostname=in_hostname INTO @host_id;
	IF @host_id IS NULL THEN
		SELECT NULL AS group_id, NULL AS host_id, "Host not found" AS error;
		LEAVE jproc;
	END IF;
	DELETE FROM group_hosts WHERE group_id=@group_id AND host_id=@host_id LIMIT 1;
	SELECT @group_id AS group_id, @host_id AS host_id;
END$$

DELIMITER ;
