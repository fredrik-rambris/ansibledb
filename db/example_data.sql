INSERT INTO `groups` (`name`) VALUES ('backoffice');
INSERT INTO `groups` (`name`) VALUES ('wizbang-backoffice');
CALL ADD_GROUP_TO_GROUP('backoffice', 'wizbang-backoffice');

INSERT INTO `groups` (`name`) VALUES ('frontapp');
INSERT INTO `groups` (`name`) VALUES ('wizbang-frontapp');
CALL ADD_GROUP_TO_GROUP('frontapp', 'wizbang-frontapp');

INSERT INTO `groups` (`name`) VALUES ('redis');
INSERT INTO `groups` (`name`) VALUES ('wizbang-redis');
CALL ADD_GROUP_TO_GROUP('redis', 'wizbang-redis');

INSERT INTO `groups` (`name`) VALUES ('mongodb');
INSERT INTO `groups` (`name`) VALUES ('wizbang-mongodb');
CALL ADD_GROUP_TO_GROUP('mongodb', 'wizbang-mongodb');

INSERT INTO `groups` (`name`) VALUES ('mongodb-cluster');
CALL ADD_GROUP_TO_GROUP('mongodb-cluster', 'wizbang-mongodb');

INSERT INTO `groups` (`name`) VALUES ('web');
INSERT INTO `groups` (`name`) VALUES ('wizbang-web');
CALL ADD_GROUP_TO_GROUP('web', 'wizbang-web');


INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv001.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv002.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv003.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv004.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv005.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv006.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv007.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv008.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv009.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv010.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv011.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv012.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv013.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv014.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv015.useast.wizbang.corp');
INSERT INTO `hosts` (`hostname`) VALUES ('prodsrv016.useast.wizbang.corp');

CALL SET_HOST_VAR('prodsrv001.useast.wizbang.corp', 'ansible_host', '10.47.13.101');
CALL SET_HOST_VAR('prodsrv002.useast.wizbang.corp', 'ansible_host', '10.47.13.102');
CALL SET_HOST_VAR('prodsrv003.useast.wizbang.corp', 'ansible_host', '10.47.13.103');
CALL SET_HOST_VAR('prodsrv004.useast.wizbang.corp', 'ansible_host', '10.47.13.104');
CALL SET_HOST_VAR('prodsrv005.useast.wizbang.corp', 'ansible_host', '10.47.13.105');
CALL SET_HOST_VAR('prodsrv006.useast.wizbang.corp', 'ansible_host', '10.47.13.106');
CALL SET_HOST_VAR('prodsrv007.useast.wizbang.corp', 'ansible_host', '10.47.13.107');
CALL SET_HOST_VAR('prodsrv008.useast.wizbang.corp', 'ansible_host', '10.47.13.108');
CALL SET_HOST_VAR('prodsrv009.useast.wizbang.corp', 'ansible_host', '10.47.13.109');
CALL SET_HOST_VAR('prodsrv010.useast.wizbang.corp', 'ansible_host', '10.47.13.110');
CALL SET_HOST_VAR('prodsrv011.useast.wizbang.corp', 'ansible_host', '10.47.13.111');
CALL SET_HOST_VAR('prodsrv012.useast.wizbang.corp', 'ansible_host', '10.47.13.112');
CALL SET_HOST_VAR('prodsrv013.useast.wizbang.corp', 'ansible_host', '10.47.13.113');
CALL SET_HOST_VAR('prodsrv014.useast.wizbang.corp', 'ansible_host', '10.47.13.114');
CALL SET_HOST_VAR('prodsrv015.useast.wizbang.corp', 'ansible_host', '10.47.13.115');
CALL SET_HOST_VAR('prodsrv016.useast.wizbang.corp', 'ansible_host', '10.47.13.116');

CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv001.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv002.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv003.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv004.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv005.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv006.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv007.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv008.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv009.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-web', 'prodsrv010.useast.wizbang.corp');

CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv001.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv002.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv003.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv004.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv005.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv006.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv007.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-frontapp', 'prodsrv008.useast.wizbang.corp');

CALL ADD_HOST_TO_GROUP('wizbang-backoffice', 'prodsrv009.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-backoffice', 'prodsrv010.useast.wizbang.corp');

CALL ADD_HOST_TO_GROUP('wizbang-mongodb', 'prodsrv011.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-mongodb', 'prodsrv012.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-mongodb', 'prodsrv013.useast.wizbang.corp');

CALL ADD_HOST_TO_GROUP('wizbang-redis', 'prodsrv014.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-redis', 'prodsrv015.useast.wizbang.corp');
CALL ADD_HOST_TO_GROUP('wizbang-redis', 'prodsrv016.useast.wizbang.corp');

CALL SET_GROUP_VAR('web', 'web_rpm_pkgs', '- httpd\n- mod_ssl');
CALL SET_GROUP_VAR('frontapp', 'frontapp_rpm_pkgs', '- php\n- php-json\n- php_mongodb\n- php-phpiredis');
CALL SET_GROUP_VAR('backoffice', 'backoffice_rpm_pkgs', '- java-1.8.0-openjdk');
CALL SET_GROUP_VAR('mongodb', 'mongodb_rpm_pkgs', '- mongodb-server\n- mongodb\n- mongodb-tools');
CALL SET_GROUP_VAR('redis', 'redis_rpm_pkgs', '- redis');
